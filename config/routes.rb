Rails.application.routes.draw do

  root 'welcome#index'
  get 'puzzle' => 'puzzles#show_puzzles'
  get 'tech' => 'puzzles#show_techs'

  # # Sessions (Login/Logout)
  # post '/login' => 'session#create', as: :login
  # get 'logout' => 'sessions#destroy', as: :logout

  # User Panel
  get '/dashboard' => 'users#dashboard', as: :dashboard

  resources :user


  # Admin Panel
  namespace :admin do
    # Dashboard
    get '/' => 'home#index', as: :dashboard

    # Manage funds/receivers
    post '/receivers/new' => 'receivers#create', as: :create_receiver
    get '/receivers/new' => 'receivers#new', as: :new_receiver
    get '/receivers/:id/edit' => 'receivers#edit', as: :edit_receiver
    get '/receivers/:id' => 'receivers#show', as: :receiver
    patch '/receivers/:id' => 'receivers#update', as: :update_receiver
    delete '/receivers/:id' => 'receivers#destroy', as: :destroy_receiver
    post '/receivers/gift' => 'receivers#gift', as: :gift_receiver
    # get '/receivers/gift/:id' => 'receivers#gift_data', as: :gift_data
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
