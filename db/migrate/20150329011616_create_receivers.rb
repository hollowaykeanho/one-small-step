class CreateReceivers < ActiveRecord::Migration
  def change
    create_table :receivers do |t|
      t.string :ngo_name, null: false, unique: true
      t.string :name, limit: 24 # Required when ReceivingCard account provided, string, 1-24
      t.string :phone # Optional, numeric, 1-20
      t.string :account_number # Conditional, Numeric, 11-19
      t.timestamps null: false
    end
  end
end
