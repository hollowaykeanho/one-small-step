class CreateStartupSettings < ActiveRecord::Migration
  def change
    create_table :startup_settings do |t|
      t.integer :funds
      t.string :sender_name, limit: 24 #Required when FundingCard account provided, string, 1-24
      # Address
      t.string :address_line1, limit: 50 #Conditional, Alpha Numeric Special, 1-50
      t.string :address_line2, limit: 50 #Optional, Alpha Numeric Special, 1-50
      t.string :address_city, limit: 25 #Conditional, string, 1-25
      t.string :address_country_subdivision, limit: 3 #Conditional, string, 2-3
      t.string :address_postal_code, limit: 10 #Conditional, string, 1-10
      t.string :address_country, limit: 3 #Conditional, string, 3
      # Card
      t.string :card_account_number #Conditional, numeric, 11-19
      t.string :card_expiry_month #Conditional, numeric, 2. Required if FundingCard is specified.
      t.string :card_expiry_year#Conditional, numeric, 4. Required if FundingCard is specified.
      t.string :ucaf, limit: 32 #Conditional, string, length 1-32.
      t.string :master_card_assigned_id #Conditional, numeric, length 6.
      t.timestamps null: false
    end
  end
end
