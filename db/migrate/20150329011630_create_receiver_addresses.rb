class CreateReceiverAddresses < ActiveRecord::Migration
  def change
    create_table :receiver_addresses do |t|
      t.string :line1, limit: 50 # Conditional, Alpha numeric special, 1-50
      t.string :line2, limit: 50 # Conditional, Alpha numeric special, 1-50
      t.string :city, limit: 25 # Conditional, string, 1-25
      t.string :country_subdivision, limit: 3 # Conditional, Alpha, 2-3
      t.string :postal_code, limit: 10 # Conditional, Alpha numeric, 1-10
      t.string :country, limit: 3 # Conditional, Alpha, 3
      t.belongs_to :receiver
      t.timestamps null: false
    end
  end
end
