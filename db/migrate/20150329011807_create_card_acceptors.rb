class CreateCardAcceptors < ActiveRecord::Migration
  def change
    create_table :card_acceptors do |t|
      t.string :name, limit: 22 #Required, string, 1-22
      t.string :city, limit: 13 #Required, string, 1-13
      t.string :state, limit: 2 #Conditional, string, 2
      t.string :postal_code, limit: 10 #Optional, string, 10
      t.string :country, limit: 3 #Required, string, 3
      t.belongs_to :receiver
      t.timestamps null: false
    end
  end
end
