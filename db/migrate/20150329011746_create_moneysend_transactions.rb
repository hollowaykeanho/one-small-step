class CreateMoneysendTransactions < ActiveRecord::Migration
  def change
    create_table :moneysend_transactions do |t|
      t.string :transaction_reference # Required, numeric, length 19.
      t.string :local_date # Required, numeric, MMDD
      t.string :local_time # Required, numeric, HHMMSS
      t.string :ica # Required, numeric, 4-6
      t.string :processor_id #Required, numeric, 10
      t.string :routing_transit #Required, numeric, 9
      t.string :channel, limit: 1, default: 'W' #Required, string, length 1
      t.string :value # Required, numeric, 2-12
      t.string :currency # Required, numeric, 3
      #t.string :transfer_type # ALWAYS RECEIVE
      t.belongs_to :receiver
      t.timestamps null: false
    end
  end
end
