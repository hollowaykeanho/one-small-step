# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts "Seeding users..."
u = User.create(email: "admin@admin.com",
            password: "admin",
            first_name: "admin",
            last_name: "admin",
            date_of_birth: Date.parse("28/03/2015"),
            role: 2) #0: normal user, 1: subscriber, 2: admin
p u

puts "Seeding startup settings..."
settings = StartupSetting.create!(funds: 2147483647,
                      sender_name: "John Doe",
                      address_line1: "123 Main Street",
                      address_line2: "#5A",
                      address_city: "Arlington",
                      address_country_subdivision: "VA",
                      address_postal_code: "22207",
                      address_country: "USA",
                      card_account_number: "5184680430000261",
                      card_expiry_month: "11",
                      card_expiry_year: "2016",
                      ucaf: "MjBjaGFyYWN0ZXJqdW5rVUNBRjU=#{to_s[0..3]}",
                      master_card_assigned_id: Random.new_seed.to_s[0..5])
p settings

puts "Seeding receiver settings..."
10.times do
  begin
  rec = Receiver.create!(ngo_name: Faker::Company.name,
                  name: Faker::Name.name,
                  phone: "1800639426",
                  account_number: "5184680430000279")

  rec.receiver_address = ReceiverAddress.create!(line1: "Pueblo Street",
                              line2:"PO BOX 12",
                              city: "El PASO",
                              country_subdivision: "TX",
                              postal_code: "79906",
                              country: "USA")
  rec.card_acceptor = CardAcceptor.create!(name: "Demo Bank",
                                          city: "The Bank City",
                                          state: "MA",
                                          postal_code: "63101",
                                          country: "USA")
  rescue
    puts rec.errors.full_messages
  end
end