# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150329011807) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "card_acceptors", force: :cascade do |t|
    t.string   "name",        limit: 22
    t.string   "city",        limit: 13
    t.string   "state",       limit: 2
    t.string   "postal_code", limit: 10
    t.string   "country",     limit: 3
    t.integer  "receiver_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "moneysend_transactions", force: :cascade do |t|
    t.string   "transaction_reference"
    t.string   "local_date"
    t.string   "local_time"
    t.string   "ica"
    t.string   "processor_id"
    t.string   "routing_transit"
    t.string   "channel",               limit: 1, default: "W"
    t.string   "value"
    t.string   "currency"
    t.integer  "receiver_id"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  create_table "receiver_addresses", force: :cascade do |t|
    t.string   "line1",               limit: 50
    t.string   "line2",               limit: 50
    t.string   "city",                limit: 25
    t.string   "country_subdivision", limit: 3
    t.string   "postal_code",         limit: 10
    t.string   "country",             limit: 3
    t.integer  "receiver_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "receivers", force: :cascade do |t|
    t.string   "ngo_name",                  null: false
    t.string   "name",           limit: 24
    t.string   "phone"
    t.string   "account_number"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "startup_settings", force: :cascade do |t|
    t.integer  "funds"
    t.string   "sender_name",                 limit: 24
    t.string   "address_line1",               limit: 50
    t.string   "address_line2",               limit: 50
    t.string   "address_city",                limit: 25
    t.string   "address_country_subdivision", limit: 3
    t.string   "address_postal_code",         limit: 10
    t.string   "address_country",             limit: 3
    t.string   "card_account_number"
    t.string   "card_expiry_month"
    t.string   "card_expiry_year"
    t.string   "ucaf",                        limit: 32
    t.string   "master_card_assigned_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                     null: false
    t.string   "password",                  null: false
    t.string   "password_salt",             null: false
    t.string   "first_name",                null: false
    t.string   "last_name",                 null: false
    t.date     "date_of_birth",             null: false
    t.integer  "role",          default: 0, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
