class Receiver < ActiveRecord::Base
  has_one :receiver_address
  has_one :card_acceptor
  has_many :moneysend_transactions
  validates :ngo_name, uniqueness: true
  validates :name, length: { minimum: 1, maximum: 24 }
  validates :phone, length: { minimum: 1, maximum: 20 }
  validates :account_number, length: { minimum: 11, maximum: 19 }

  def gift(amount)
    transaction_reference = "5#{[*0..9, *0..9, *0..9, *0..9].sample(18).join}"
    p transaction_reference
    startup = StartupSetting.first
    transfer_request = {}
    transfer_request["LocalDate"] = Time.now.strftime('%m%d')
    transfer_request["LocalTime"] = Time.now.strftime('%H%M%S')
    transfer_request["TransactionReference"] = Random.new_seed.to_s[0..18]
    # Sender data

    transfer_request["SenderName"] = startup.sender_name
    address = {}
    address["Line1"] = startup.address_line1
    address["Line2"] = startup.address_line2
    address["City"] = startup.address_city
    address["CountrySubdivision"] = startup.address_country_subdivision
    address["PostalCode"] = startup.address_postal_code
    address["Country"] = startup.address_country
    transfer_request["SenderAddress"] = address
    card = {}
    card["AccountNumber"] = startup.card_account_number
    card["ExpiryMonth"] = startup.card_expiry_month
    card["ExpiryYear"] = startup.card_expiry_year
    transfer_request["FundingCard"] = card
    transfer_request["FundingMasterCardAssignedId"] = startup.master_card_assigned_id
    # Amount
    funding_amount = {}
    funding_amount["Value"] = amount
    funding_amount["Currency"] = "840"
    transfer_request["FundingAmount"] = funding_amount
    # Receiver data
    transfer_request["ReceiverName"] = name
    recv_address = {}
    recv_address["Line1"] = receiver_address.line1
    recv_address["Line2"] = receiver_address.line2
    recv_address["City"] = receiver_address.city
    recv_address["CountrySubdivision"] = receiver_address.country_subdivision
    recv_address["PostalCode"] = receiver_address.postal_code
    recv_address["Country"] = receiver_address.country
    transfer_request["ReceiverAddress"] = recv_address
    transfer_request["ReceiverPhone"] = phone
    recv_card = {}
    recv_card["AccountNumber"] = account_number
    transfer_request["ReceivingCard"] = recv_card
    transfer_request["ReceivingAmount"] = funding_amount
    # Others
    transfer_request["Channel"] = "W"
    transfer_request["UCAFSupport"] = "true"
    transfer_request["ICA"] = "009674"
    transfer_request["ProcessorId"] = "9000000442"
    transfer_request["RoutingAndTransitNumber"] = "990442082"
    card_accept = {}
    # For demo purposes, we're hard coding to this value. Remember to put back "card_acceptor.name"
    card_accept["Name"] =  "My Local Bank"
    card_accept["City"] = "Saint Louis"
    card_accept["State"] = "MO"
    card_accept["PostalCode"] = "63101"
    card_accept["Country"] = "USA"
    transfer_request["CardAcceptor"] = card_accept
    transfer_request["TransactionDesc"] = "P2P"
    transfer_request["MerchantId"] = "123456"

    trans = {}
    trans["TransferRequest"] = transfer_request
    p trans
    #startup.ucaf
    data = trans.to_xml.gsub("<hash>", "").gsub("</hash>", "")
    puts data
    uri = URI.parse('http://dmartin.org:8028/moneysend/v2/transfer?Format=XML')
    req = Net::HTTP.new(uri.hostname, uri.port)
    res = req.post(uri.path, data, {'Content-Type' => 'application/xml', 'Content-Length' => data.length.to_s })
    puts res.body
  end
end
