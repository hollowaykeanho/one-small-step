class CardAcceptor < ActiveRecord::Base
  belongs_to :receiver
  validates :name, presence: true, length: { minimum: 1, maximum: 22 }
  validates :city, presence: true, length: { minimum: 1, maximum: 13 }
  validates :state, presence: true, length: { is: 2 }
  validates :postal_code, length: { is: 10 }
  validates :country, presence: true, length: { is: 3 }
end