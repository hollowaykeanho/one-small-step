class User < ActiveRecord::Base
  before_save :update_password_and_role

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true,
                    uniqueness: true,
                    format: { with: VALID_EMAIL_REGEX },
                    length: { maximum: 100 }
  validates :password, presence: true, length: { minimum: 4 }
  validates :first_name, presence: true, length: { minimum: 4 }
  validates :last_name, presence: true, length: { minimum: 4 }
  validates :date_of_birth, presence: true

  def self.authenticate(params)
    user = User.find_by_email(params[:email])
    user && user.password == BCrypt::Engine.hash_secret(params[:password], user.password_salt) ? user : nil
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def subscriber?
    role == 1
  end

  def admin?
    role == 2
  end

  private
  def update_password_and_role
    role ||= 0
    password_salt = BCrypt::Engine.generate_salt
    self.password = BCrypt::Engine.hash_secret(self.password, password_salt)
    self.password_salt = password_salt
  end
end
