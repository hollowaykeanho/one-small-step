class MoneysendTransaction < ActiveRecord::Base
  belongs_to :receiver
  validates :transaction_reference, presence: true, length: { is: 19 }
  validates :local_date, presence: true, length: { is: 4 }
  validates :local_time, presence: true, length: { is: 6 }
  validates :ica, presence: true, length: { minimum: 4, maximum: 6 }
  validates :processor_id, presence: true, length: { is: 10 }
  validates :routing_transit, presence: true, length: { is: 9 }
  validates :channel, presence: true, length: { is: 1 } # W
  validates :value, presence: true, length: { minimum: 2, maximum: 12 }
  validates :currency, presence: true, length: { is: 3 }
end