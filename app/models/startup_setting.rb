class StartupSetting < ActiveRecord::Base
  validates :funds, numericality: true
  validates :sender_name, presence: true, length: { minimum: 1, maximum: 24 }
  # Address
  validates :address_line1, length: { minimum: 1, maximum: 50 }
  #validates :address_line2, length: { minimum: 1, maximum: 50 }
  validates :address_city, length: { minimum: 1, maximum: 25 }
  validates :address_country_subdivision, length: { minimum: 2, maximum: 3 }
  validates :address_postal_code, length: { minimum: 1, maximum: 10 }
  validates :address_country,  length: { is: 3 }
  # Card
  validates :card_account_number, length: { minimum: 11, maximum: 19 }
  validates :card_expiry_month, presence: true, length: { is: 2 }
  validates :card_expiry_year, presence: true, length: { is: 4 }
  validates :ucaf, length: { minimum: 1, maximum: 32 }
  validates :master_card_assigned_id,  length: { is: 6 }
end