class ReceiverAddress < ActiveRecord::Base
  belongs_to :receiver
  validates :line1, presence: true, length: { minimum: 1, maximum: 50 }
  validates :line2, presence: true, length: { minimum: 1, maximum: 50 }
  validates :city, presence: true, length: { minimum: 1, maximum: 25 }
  validates :country_subdivision, presence: true, length: { minimum: 2, maximum: 3 }
  validates :postal_code, presence: true, length: { minimum: 1, maximum: 10 }
  validates :country, presence: true, length: { is: 3 }
end