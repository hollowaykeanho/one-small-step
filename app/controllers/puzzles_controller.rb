class PuzzlesController < ApplicationController
  def show_puzzles
    render 'user/puzzle'
  end

  def show_techs
  	render 'user/tech_track'
  end

end