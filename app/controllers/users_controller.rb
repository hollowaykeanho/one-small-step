class UsersController < ApplicationController
  def dashboard
    render 'user/dashboard'
  end

  before_action :check_login!, only: [:create, :new]
  before_action :check_privileges!, only: [:edit, :update]

  def show
    @user = User.find_by_id(params[:id])
    redirect_to root_path, alert: "Invalid user id." and return unless @user
  end

  def create
    if params[:form] && (params[:form][:retype_password] != params[:form][:password])
      redirect_to new_user_path, alert: "Passwords do not match." and return
    end
    user = User.create(user_params)
    redirect_to new_user_path, alert: user.errors.full_messages and return unless user.valid?
    redirect_to login_path, notice: "Welcome on board! Successfully registered!" and return
  end

  def edit
    redirect_to root_path, alert: "Permission denied!" and return if params[:id].to_i != current_user.params[:id]
  end

  def update
    redirect_to root_path, alert: "Permission denied!" and return if params[:id].to_i != current_user.params[:id]
    user = User.find(params[:id])
    redirect_to dashboard_path, alert: "Invalid user id." and return unless user
    if params[:user] && (params[:user][:retype_password] != params[:user][:password])
      redirect_to edit_user_path(user), alert: "Passwords do not match." and return
    end
    user.update(user_params)
    redirect_to edit_user_path(user), alert: user.errors.full_messages and return unless user.valid?
    redirect_to edit_user_path(user), notice: "Successfully updated." and return
  end

  private
  def check_login!
    redirect_to dashboard_path, alert: "You are already logged in!" and return if logged_in?
  end

  def check_privileges!
    redirect_to login_path, alert: "Access forbidden! Please login to continue." and return unless logged_in?
  end

  def user_params
    params.require(:form).permit(:email, :full_name, :password, :date_of_birth)
  end


end