class Admin::ReceiversController < AdminController

  # post '/receivers/new' => 'receivers#create', as: :create_receiver_path
  def create
 p params
    redirect_to admin_dashboard_path, notice: "Successfully created a new receiver: #{@receiver.ngo_name}" and return
  end

  # get '/receivers/new' => 'receivers#new', as: :new_receiver_path
  def new
 p params
  end

  # get '/receivers/:id/edit' => 'receivers#edit', as: :edit_receiver_path
  def edit
 p params
  end

  # get '/receivers/:id' => 'receivers#show', as: :receiver_path
  def show
 p params
  end

  # patch '/receivers/:id' => 'receivers#update', as: :update_receiver_path
  def update
    p params
    redirect_to admin_dashboard_path, notice: "Successfully updated receiver: #{@receiver.ngo_name}" and return
  end

  # delete '/receivers/:id' => 'receivers#destroy', as: :destroy_receiver_path
  def destroy
 p params
    redirect_to admin_dashboard_path, notice: "Successfully deleted receiver: #{@receiver.ngo_name}" and return
  end

  # get '/receivers/gift/:id' => 'receivers#gift_data', as: :gift_data
  # def gift_data
  #   byebug
  #   p params
  #   @startup = StartupSetting.first
  #   @receiver = Receiver.find(params[:id])
  #   # transaction_reference = "5#{[*0..9, *0..9, *0..9, *0..9].sample(18).join}"
  #   # p transaction_reference
  #   # transfer_request[:TransactionReference] = transaction_reference # Random.new_seed.to_s[0..18]

  #   # transfer_request[:Channel] = "W"
  #   # transfer_request[:UCAFSupport] = "true"
  #   # transfer_request[:ICA] = "009674"
  #   # transfer_request[:ProcessorId] = "9000000442"
  #   # transfer_request[:RoutingAndTransitNumber] ="990442082"

  #   # funding_amount[:Value] = amount
  #   # funding_amount[:Currency] = "840"
  #   # transfer_request[:FundingAmount] = funding_amount
  #   # transfer_request[:ReceivingAmount] = funding_amount

  #   render :gift_data
  # end

  # post '/receivers/gift' => 'receivers#gift', as: :gift_receiver_path
  def gift
    @amount = params[:gift][:amount].to_i
    if StartupSetting.first.funds < @amount
      redirect_to admin_dashboard_path, alert: "Insufficient funds." and return
    end
    @receiver = Receiver.find(params[:gift][:id])
    startup = StartupSetting.first
    startup.funds -= @amount
    startup.save
    @receiver.gift(@amount)
    p @receiver
    redirect_to admin_dashboard_path, notice: "Successfully gifted receiver: #{@receiver.ngo_name} USD #{@amount}" and return
  end
end