class AdminController < ApplicationController
  before_action :require_admin

  layout "application_admin"

  private
  def require_admin
    session[:user_id] = User.first.id
    redirect_to root_path, alert: "Access forbidden!"  unless logged_in? && current_user.admin?
  end
end