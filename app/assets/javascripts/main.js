$( document ).ready(function() {
    console.log( "ready!" );

    var option1 = "Facebook, Instagram, Snapchat";
    var option1ext = " (Incorrect, these are social media :))";
    var option2 = "3G, 4G LTE";
    var option2ext = " (Incorrect, 3G and 4G are the bandwidth the connection is running.)";
    var option3 = "Interconnected network of cables";
    var option3ext = " (Correct! Internet are arrays of cables around the world that are interconnected.)";
    var option4 = "Online Shopping";
    var option4ext = " (Incorrect, online shopping is another application of Internet)";

    $('#r5').click(function() {
	   	if($('#r5').is(':checked')) { 
	   		$("#r5label").html($("#r5label").html() + option1ext);
		   	$("#r6label").html(option2);
		   	$("#r7label").html(option3);
		   	$("#r8label").html(option4);
	   	}
	});
    $('#r6').click(function() {
	   	if($('#r6').is(':checked')) { 
	   		$("#r5label").html(option1);
		   	$("#r6label").html($("#r6label").html() + option2ext);
		   	$("#r7label").html(option3);
		   	$("#r8label").html(option4);
	   	}
	});
	$('#r7').click(function() {
	   	if($('#r7').is(':checked')) { 
	   		$("#r5label").html(option1);
		   	$("#r6label").html(option2);
		   	$("#r7label").html($("#r7label").html() + option3ext);
		   	$("#r8label").html(option4);
	   	}
	});
	$('#r8').click(function() {
	   	if($('#r8').is(':checked')) { 
	   		$("#r5label").html(option1);
		   	$("#r6label").html(option2);
		   	$("#r7label").html(option3);
		   	$("#r8label").html($("#r8label").html() + option4ext);
	   	}
	});
});

